#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Transpose {
  N,
  T,
  H,
}

pub mod blas {

pub use super::{Transpose};

pub trait Blas<'a, T> where T: 'a + Copy {
  type Ctx;
}

/// An interface to BLAS1 routines where the output is a scalar.
pub trait BScalar<'a, T>: Blas<'a, T> where T: 'a + Copy {
  // TODO
  //fn inner_prod(&mut self, alpha: T, x: &Self::Vector, y: &Self::Vector, ctx: &Self::Ctx);
}

/// An interface to BLAS1 and BLAS2 routines where the output is a vector.
pub trait BVector<'a, T>: Blas<'a, T> where T: 'a + Copy {
  type Matrix;
  type Vector;

  fn row_vector_scale(&mut self, alpha: T, ctx: &Self::Ctx) { unimplemented!(); }
  fn col_vector_sum(&mut self, alpha: T, x: &Self::Vector, ctx: &Self::Ctx) { unimplemented!(); }
  fn row_vector_sum(&mut self, alpha: T, x: &Self::Vector, ctx: &Self::Ctx) { unimplemented!(); }
  fn scalar_vector_prod(&mut self, alpha: T, ctx: &Self::Ctx) { unimplemented!(); }
  fn matrix_vector_prod(&mut self, alpha: T, a: &Self::Matrix, trans_a: Transpose, x: &Self::Vector, ctx: &Self::Ctx) { unimplemented!(); }
}

/// An interface to BLAS3 routines where the output is a matrix.
pub trait BMatrix<'a, T>: Blas<'a, T> where T: 'a + Copy {
  type Matrix;
  type Vector;

  fn matrix_scale(&mut self, alpha: f32, ctx: &Self::Ctx) { unimplemented!(); }
  fn matrix_sum(&mut self, alpha: T, x: &Self::Matrix, ctx: &Self::Ctx) { unimplemented!(); }
  fn scalar_matrix_prod(&mut self, alpha: T, a: &Self::Vector, ctx: &Self::Ctx) { unimplemented!(); }
  fn rank_one_update(&mut self, x: &Self::Vector, y: &Self::Vector, ctx: &Self::Ctx) { unimplemented!(); }
  fn hadamard_prod(&mut self, a: &Self::Matrix, ctx: &Self::Ctx) { unimplemented!(); }
  fn matrix_prod(&mut self, alpha: T, a: &Self::Matrix, trans_a: Transpose, b: &Self::Matrix, trans_b: Transpose, beta: T, ctx: &Self::Ctx) { unimplemented!(); }
  fn diag_matrix_prod(&mut self, diag: &Self::Vector, a: &Self::Matrix, ctx: &Self::Ctx) { unimplemented!(); }
}

} // pub mod blas

pub mod sparse {

use super::{Transpose};

pub trait Sparse<'a, T> where T: 'a + Copy {
  type Ctx;
}

pub trait SpVectorOps<'a, T>: Sparse<'a, T> where T: 'a + Copy {
  type DenseMatrix;
  type VectorVal;
  type VectorInd;
  type Work;

  fn matrix_sparse_vector_prod(&mut self,
      alpha: T,
      a: &Self::DenseMatrix, trans_a: Transpose,
      x_val: &Self::VectorVal,
      x_ind: &Self::VectorInd,
      x_nnz: usize,
      beta: T,
      work: &mut Self::Work,
      ctx: &Self::Ctx)
  { unimplemented!(); }
}

pub trait CsrVectorOps<'a, T>: Sparse<'a, T> where T: 'a + Copy {
  type VectorDesc;
  type VectorVal;
  type VectorRowPtr;
  type VectorColInd;
  type VectorRowNnz;

  fn into_csr_vector(&self,
      x_desc: &Self::VectorDesc,
      x_val: &mut Self::VectorVal,
      x_row_ptr: &mut Self::VectorRowPtr,
      x_col_ind: &mut Self::VectorColInd,
      x_row_nnz: &mut Self::VectorRowNnz,
      x_nnz: &mut usize,
      ctx: &Self::Ctx)
  { unimplemented!(); }
}

pub trait CsrVectorMutOps<'a, T>: Sparse<'a, T> where T: 'a + Copy {
  type DenseMatrix;
  type VectorDesc;
  type VectorVal;
  type VectorRowPtr;
  type VectorColInd;

  fn matrix_csr_vector_prod(&mut self,
      alpha: T,
      x_desc: &Self::VectorDesc,
      x_val: &Self::VectorVal,
      x_row_ptr: &Self::VectorRowPtr,
      x_col_ind: &Self::VectorColInd,
      x_nnz: usize,
      b: &Self::DenseMatrix,
      beta: T,
      ctx: &Self::Ctx)
  { unimplemented!(); }
}

} // pub mod sparse
